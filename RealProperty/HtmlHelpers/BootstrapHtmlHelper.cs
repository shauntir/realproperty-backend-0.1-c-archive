﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace RealProperty.HtmlHelpers
{

    public static class BootstrapHtmlHelper
    {
        public static MvcHtmlString ClassForNavListLink(this HtmlHelper htmlHelper, string listActionName, string listControllerName)
        {
            string requestActionName = htmlHelper.ViewContext.RouteData.Values["action"].ToString();
            string requestControllerName = htmlHelper.ViewContext.RouteData.Values["controller"].ToString();

            if (listControllerName.Equals(requestControllerName, StringComparison.OrdinalIgnoreCase) &&
                listActionName.Equals(requestActionName, StringComparison.OrdinalIgnoreCase))
            {
                return MvcHtmlString.Create("active");
            }
            else
            {
                return MvcHtmlString.Empty;
            }
        }

        public static MvcHtmlString ClassForNavListLink(this HtmlHelper htmlHelper, string listControllerName, string[] listActionNames)
        {
            string requestActionName = htmlHelper.ViewContext.RouteData.Values["action"].ToString();
            string requestControllerName = htmlHelper.ViewContext.RouteData.Values["controller"].ToString();

            if (listControllerName.Equals(requestControllerName, StringComparison.OrdinalIgnoreCase) && Array.IndexOf(listActionNames, requestActionName) > -1)
            {
                return MvcHtmlString.Create("active");
            }
            else
            {
                return MvcHtmlString.Empty;
            }
        }

        public static MvcHtmlString FormControlValidatorClassFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            string modelName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));
            FormContext formContext = htmlHelper.ViewContext.ClientValidationEnabled ? htmlHelper.ViewContext.FormContext : null;

            if (!htmlHelper.ViewData.ModelState.ContainsKey(modelName) && formContext == null)
            {
                return null;
            }

            ModelState modelState = htmlHelper.ViewData.ModelState[modelName];
            ModelErrorCollection modelErrors = (modelState == null) ? null : modelState.Errors;
            ModelError modelError = ((modelErrors == null) || (modelErrors.Count == 0)) ? null : modelErrors[0];

            if (modelError != null)
            {
                return MvcHtmlString.Create("has-error");
            }
            return null;
        }
    }
}