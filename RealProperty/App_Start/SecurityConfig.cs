﻿using RealProperty.Domain.Models;
using System.Data.Entity;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using System;

namespace RealProperty
{
    public class SecurityConfig
    {
        private static MembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        public static void RegisterWebSecurity()
        {
            var init = new RealPropertyInitializer();
            Database.SetInitializer<RealPropertyContext>(init);

            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        public class MembershipInitializer 
        {
            public MembershipInitializer()
            {
                using (var db = new RealPropertyContext()) 
                {
                    //if (!db.Database.Exists())
                    //{
                    //    throw new InvalidOperationException("Database does not exist or could not be found.");
                    //}
                    db.Users.Load();
                }
                
                if (!WebSecurity.Initialized)
                {
                    WebSecurity.InitializeDatabaseConnection("RealProp", "User", "UserId", "UserName", autoCreateTables: true);
                }
            }
        }
    }
} 