﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;
using WebMatrix.WebData;
using Microsoft.Web.WebPages.OAuth;
using System.Web.Security;
using RealProperty.Domain.Helpers;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman")]
    public class UserController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            var users = db.Users.ToList();
            if (WebSecurity.IsAuthenticated)
            {
                return RedirectToAction(returnUrl);
            }
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLogin model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                WebSecurity.Login(model.UserName, model.Password);
                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        //
        // POST: /Account/LogOff

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }


        private void PopulateFields()
        {
            int? currentUserId = DataAccess.GetCurrentUserId();
            var user = (from u in db.Users
                        where u.UserId == currentUserId
                        select u).FirstOrDefault();

            ViewBag.ShowField = false;
            if (Roles.IsUserInRole(user.UserName, "Superman"))
            {
                ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name", user.CompanyId);
                ViewBag.ShowField = true;
            }
        }
        //
        // GET: /User/Details/5

        public ActionResult Details(int id = 0)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            PopulateFields();
            return View(user);
        }

        //
        // GET: /User/Create
        public ActionResult Create()
        {
            PopulateFields();
            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserCreate user)
        {
            if (ModelState.IsValid)
            {
                // Attempt to create the user
                try
                {
                    WebSecurity.CreateUserAndAccount(user.UserName, user.Password, new { user.Email, user.FirstName, user.LastName, user.CompanyId, user.DateTimeModified });
                    return RedirectToAction("Index");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
                return RedirectToAction("Index");
            }

            PopulateFields();
            return View(user);
        }

        //
        // GET: /User/Edit/5

        public ActionResult Edit(int id = 0)
        {
            User user = db.Users.Find(id);

            AutoMapper.Mapper.CreateMap<User, EditUserViewModel>();
            var userEdit = AutoMapper.Mapper.Map<User, EditUserViewModel>(user);

            if (user == null)
            {
                return HttpNotFound();
            }

            PopulateFields();
            return View(userEdit);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditUserViewModel editUser)
        {
            AutoMapper.Mapper.CreateMap<EditUserViewModel, User>();
            var user = AutoMapper.Mapper.Map<EditUserViewModel, User>(editUser);

            if (ModelState.IsValid)
            {
                db.Users.Attach(user);
                db.Entry<User>(user).Property(u => u.UserName).IsModified = true;
                db.Entry<User>(user).Property(u => u.FirstName).IsModified = true;
                db.Entry<User>(user).Property(u => u.LastName).IsModified = true;
                db.Entry<User>(user).Property(u => u.Email).IsModified = true;
                db.Entry<User>(user).Property(u => u.CompanyId).IsModified = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            PopulateFields();
            return View(editUser);
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}