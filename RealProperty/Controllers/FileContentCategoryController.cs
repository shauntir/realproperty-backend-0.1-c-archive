﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman")]
    public class FileContentCategoryController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /FileContentCategory/

        public ActionResult Index()
        {
            return View(db.FileContentCategories.ToList());
        }

        //
        // GET: /FileContentCategory/Details/5

        public ActionResult Details(int id = 0)
        {
            FileContentCategory filecontentcategory = db.FileContentCategories.Find(id);
            if (filecontentcategory == null)
            {
                return HttpNotFound();
            }
            return View(filecontentcategory);
        }

        //
        // GET: /FileContentCategory/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /FileContentCategory/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FileContentCategory filecontentcategory)
        {
            if (ModelState.IsValid)
            {
                db.FileContentCategories.Add(filecontentcategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(filecontentcategory);
        }

        //
        // GET: /FileContentCategory/Edit/5

        public ActionResult Edit(int id = 0)
        {
            FileContentCategory filecontentcategory = db.FileContentCategories.Find(id);
            if (filecontentcategory == null)
            {
                return HttpNotFound();
            }
            return View(filecontentcategory);
        }

        //
        // POST: /FileContentCategory/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FileContentCategory filecontentcategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(filecontentcategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(filecontentcategory);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}