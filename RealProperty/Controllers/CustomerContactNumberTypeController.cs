﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman")]
    public class CustomerContactNumberTypeController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /ContactNumberType/

        public ActionResult Index()
        {
            return View(db.ContactNumberTypes.ToList());
        }

        //
        // GET: /ContactNumberType/Details/5

        public ActionResult Details(int id = 0)
        {
            ContactNumberType customercontactnumbertype = db.ContactNumberTypes.Find(id);
            if (customercontactnumbertype == null)
            {
                return HttpNotFound();
            }
            return View(customercontactnumbertype);
        }

        //
        // GET: /ContactNumberType/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ContactNumberType/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ContactNumberType customercontactnumbertype)
        {
            if (ModelState.IsValid)
            {
                db.ContactNumberTypes.Add(customercontactnumbertype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(customercontactnumbertype);
        }

        //
        // GET: /ContactNumberType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ContactNumberType customercontactnumbertype = db.ContactNumberTypes.Find(id);
            if (customercontactnumbertype == null)
            {
                return HttpNotFound();
            }
            return View(customercontactnumbertype);
        }

        //
        // POST: /ContactNumberType/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ContactNumberType customercontactnumbertype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customercontactnumbertype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customercontactnumbertype);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}