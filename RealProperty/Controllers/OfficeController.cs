﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman, Admin")]
    public class OfficeController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /Office/

        public ActionResult Index()
        {
            var offices = db.Offices.Include(o => o.Address);
            return View(offices.ToList());
        }

        //
        // GET: /Office/Details/5

        public ActionResult Details(int id = 0)
        {
            Office office = db.Offices.Find(id);
            if (office == null)
            {
                return HttpNotFound();
            }
            ViewBag.MainOfficeId = new SelectList(db.Offices, "OfficeId", "Name", office.MainOfficeId);
            ViewBag.AddressTypeId = new SelectList(db.AddressTypes, "AddressTypeId", "Description");
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "Description");
            return View(office);
        }

        //
        // GET: /Office/Create

        public ActionResult Create()
        {
            ViewBag.MainOfficeId = new SelectList(db.Offices, "OfficeId", "Name");
            ViewBag.AddressTypeId = new SelectList(db.AddressTypes, "AddressTypeId", "Description");
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "Description");
            return View();
        }

        private void PopulateEditFields(Office office)
        {
            var offices = db.Offices.Where(o => o.OfficeId != office.OfficeId);
            ViewBag.MainOfficeId = new SelectList(offices, "OfficeId", "Name", office.MainOfficeId);
            ViewBag.AddressTypeId = new SelectList(db.AddressTypes, "AddressTypeId", "Description");
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "Description");
        }
        //
        // POST: /Office/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Office office)
        {
            if (ModelState.IsValid)
            {
                db.Offices.Add(office);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            PopulateEditFields(office);
            return View(office);
        }

        //
        // GET: /Office/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Office office = db.Offices.Find(id);
            if (office == null)
            {
                return HttpNotFound();
            }
            PopulateEditFields(office);
            return View(office);
        }

        //
        // POST: /Office/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Office office)
        {
            var editMainOffice = db.Offices.Where(o => o.OfficeId == office.MainOfficeId).FirstOrDefault();
            if (editMainOffice != null)
            {
                db.Entry(editMainOffice).State = EntityState.Detached;

                if (editMainOffice.MainOfficeId == office.OfficeId)
                {
                    ModelState.AddModelError("Cycle reference error", "Cannot save main office since it already has this office as its main office.");
                }
            }

            if (ModelState.IsValid)
            {
                db.Entry(office.Address).State = EntityState.Modified;
                db.Entry(office).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PopulateEditFields(office);
            return View(office);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}