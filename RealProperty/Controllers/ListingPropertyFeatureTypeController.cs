﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman")]
    public class ListingPropertyFeatureTypeController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /ListingPropertyFeatureType/

        public ActionResult Index()
        {
            return View(db.ListingPropertyFeatureTypes.ToList());
        }

        //
        // GET: /ListingPropertyFeatureType/Details/5

        public ActionResult Details(int id = 0)
        {
            ListingPropertyFeatureType listingpropertyfeaturetype = db.ListingPropertyFeatureTypes.Find(id);
            if (listingpropertyfeaturetype == null)
            {
                return HttpNotFound();
            }
            return View(listingpropertyfeaturetype);
        }

        //
        // GET: /ListingPropertyFeatureType/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ListingPropertyFeatureType/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ListingPropertyFeatureType listingpropertyfeaturetype)
        {
            if (ModelState.IsValid)
            {
                db.ListingPropertyFeatureTypes.Add(listingpropertyfeaturetype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(listingpropertyfeaturetype);
        }

        //
        // GET: /ListingPropertyFeatureType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ListingPropertyFeatureType listingpropertyfeaturetype = db.ListingPropertyFeatureTypes.Find(id);
            if (listingpropertyfeaturetype == null)
            {
                return HttpNotFound();
            }
            return View(listingpropertyfeaturetype);
        }

        //
        // POST: /ListingPropertyFeatureType/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ListingPropertyFeatureType listingpropertyfeaturetype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(listingpropertyfeaturetype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(listingpropertyfeaturetype);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}