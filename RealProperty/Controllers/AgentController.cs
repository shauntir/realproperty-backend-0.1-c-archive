﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;
using System.Web.Security;
using RealProperty.Domain.Helpers;
using WebMatrix.WebData;
using RealProperty.Domain.ViewModels;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman, Admin")]
    public class AgentController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        private void PopulateFields()
        {
            int? currentUserId = DataAccess.GetCurrentUserId();
            var user = (from u in db.Users
                        where u.UserId == currentUserId
                        select u).FirstOrDefault();

            ViewBag.ShowField = false;
            if (Roles.IsUserInRole(user.UserName, "Superman"))
            {
                ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name", user.CompanyId);
                ViewBag.ShowField = true;
            }
        }

        public int GetCurrentUserCompanyId()
        {
            try
            {
                int? currenUserId = DataAccess.GetCurrentUserId();
                var companyId = db.Users.Where(u => u.UserId == currenUserId).Select(u => u.CompanyId).FirstOrDefault();
                return companyId;
            }
            catch (Exception)
            {
                return 1;
            }
        }

        //
        // GET: /Agent/

        public ActionResult Index()
        {
            var agents = db.Agents.Include(a => a.Office).Include(a => a.User);
            return View(agents.ToList());
        }

        //
        // GET: /Agent/Details/5

        public ActionResult Details(int id = 0)
        {
            Agent agent = db.Agents.Find(id);
            
            AutoMapper.Mapper.CreateMap<User, EditAgentViewModel>();
            var agentEdit = AutoMapper.Mapper.Map<User, EditAgentViewModel>(agent.User);
            agentEdit.AgentId = agent.AgentId;
            agentEdit.OfficeId = agent.OfficeId;

            if (agent == null)
            {
                return HttpNotFound();
            }

            ViewBag.OfficeId = new SelectList(db.Offices, "OfficeId", "Name", agent.OfficeId);
            PopulateFields();
            return View(agentEdit);
        }

        //
        // GET: /Agent/Create

        public ActionResult Create()
        {
            CreateAgentViewModel agent = new CreateAgentViewModel();

            var contactNumbers = (from c in db.ContactNumberTypes 
                                  select new ContactNumberViewModel
                                  {
                                      ContactNumberTypeId = c.ContactNumberTypeId,
                                      Description = c.Description,
                                      ContactNumber = String.Empty
                                  }).ToList();

            agent.ContactNumberViewModels = contactNumbers;

            ViewBag.OfficeId = new SelectList(db.Offices, "OfficeId", "Name");
            PopulateFields();
            return View(agent);
        }

        //
        // POST: /Agent/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateAgentViewModel createAgent)
        {
            if (ModelState.IsValid)
            {
                AutoMapper.Mapper.CreateMap<CreateAgentViewModel, UserCreate>();
                var userCreate = AutoMapper.Mapper.Map<CreateAgentViewModel, UserCreate>(createAgent);
                if (userCreate.CompanyId == 0)
                {
                    userCreate.CompanyId = GetCurrentUserCompanyId();
                }
                
                AutoMapper.Mapper.CreateMap<UserCreate, User>();
                var user = AutoMapper.Mapper.Map<UserCreate, User>(userCreate);

                var agent = new Agent();
                agent.OfficeId = createAgent.OfficeId;
                agent.User = user;

                foreach (var contactNumbers in createAgent.ContactNumberViewModels.Where(c => c.ContactNumber != null))
                {
                    var a = new AgentContactNumber
                    {
                        ContactNumberTypeId = contactNumbers.ContactNumberTypeId,
                        ContactNumber = contactNumbers.ContactNumber,
                        AgentId = agent.AgentId
                    };
                    db.AgentContactNumbers.Add(a);
                }

                db.Agents.Add(agent);
                db.SaveChanges();
                WebSecurity.CreateAccount(userCreate.UserName, userCreate.Password);
                Roles.AddUsersToRole(new[] { userCreate.UserName }, "Admin");

                return RedirectToAction("Index");
            }

            ViewBag.OfficeId = new SelectList(db.Offices, "OfficeId", "Name");
            PopulateFields();
            return View(createAgent);
        }

        //
        // GET: /Agent/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Agent agent = db.Agents.Find(id);

            AutoMapper.Mapper.CreateMap<User, EditAgentViewModel>();
            var agentEdit = AutoMapper.Mapper.Map<User, EditAgentViewModel>(agent.User);
            agentEdit.AgentId = agent.AgentId;
            agentEdit.OfficeId = agent.OfficeId;

            if (agent == null)
            {
                return HttpNotFound();
            }

            var contacts = (from c in db.ContactNumberTypes 
                            join ac in db.AgentContactNumbers on
                            c.ContactNumberTypeId equals ac.ContactNumberTypeId into conts
                            from cont in conts.DefaultIfEmpty()
                            where cont.AgentId == agent.AgentId
                            select new ContactNumberViewModel 
                            {
                                ContactNumberTypeId = c.ContactNumberTypeId,
                                Description = c.Description,
                                ContactNumber = cont.ContactNumber
                            }).ToList();

            agentEdit.ContactNumberViewModels = contacts;

            ViewBag.OfficeId = new SelectList(db.Offices, "OfficeId", "Name", agent.OfficeId);
            PopulateFields();
            return View(agentEdit);
        }

        //
        // POST: /Agent/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditAgentViewModel editAgent)
        {
            if (ModelState.IsValid)
            {
                AutoMapper.Mapper.CreateMap<EditAgentViewModel, Agent>();
                var agent = AutoMapper.Mapper.Map<EditAgentViewModel, Agent>(editAgent);
                
                AutoMapper.Mapper.CreateMap<EditAgentViewModel, User>();
                var user = AutoMapper.Mapper.Map<EditAgentViewModel, User>(editAgent);
                user.UserId = db.Agents.Where(a => a.AgentId == agent.AgentId).Select(a => a.UserId).FirstOrDefault();

                db.Users.Attach(user);
                db.Entry<User>(user).Property(u => u.UserName).IsModified = true;
                db.Entry<User>(user).Property(u => u.FirstName).IsModified = true;
                db.Entry<User>(user).Property(u => u.LastName).IsModified = true;
                db.Entry<User>(user).Property(u => u.Email).IsModified = true;
                db.Entry<User>(user).Property(u => u.CompanyId).IsModified = true;
                db.SaveChanges();

                agent.UserId = user.UserId;
                db.Entry(agent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OfficeId = new SelectList(db.Companies, "OfficeId", "Name", editAgent.OfficeId);
            PopulateFields();
            return View(editAgent);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}