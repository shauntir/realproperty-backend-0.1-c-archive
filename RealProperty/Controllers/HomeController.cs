﻿using RealProperty.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman, Admin")]
    public class HomeController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        public ActionResult Index()
        {
            var listings = db.ListingProperties.Where(l => l.InActive == false).ToList();

            return View(listings);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult AdminLookups()
        {
            ViewBag.Message = "Admin Lookups.";

            return View();
        }

        public ActionResult PropertyManagement()
        {
            ViewBag.Message = "Property Management.";

            return View();
        }

        public ActionResult ContentManagement()
        {
            ViewBag.Message = "Content Management.";

            return View();
        }

        public ActionResult CustomerManagement()
        {
            ViewBag.Message = "Customer Management.";

            return View();
        }

        public ActionResult AgentManagement()
        {
            ViewBag.Message = "Agent Management.";

            return View();
        }

        public ActionResult AccessDenied()
        {
            ViewBag.Message = "Sorry, you cannot access this URL. Please contact the system administrator if you do need access.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
