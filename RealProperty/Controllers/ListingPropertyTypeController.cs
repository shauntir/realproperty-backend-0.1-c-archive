﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman, Admin")]
    public class ListingPropertyTypeController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /ListingPropertyType/

        public ActionResult Index()
        {
            return View(db.ListingPropertyTypes.ToList());
        }

        //
        // GET: /ListingPropertyType/Details/5

        public ActionResult Details(int id = 0)
        {
            ListingPropertyType listingpropertytype = db.ListingPropertyTypes.Find(id);
            if (listingpropertytype == null)
            {
                return HttpNotFound();
            }
            return View(listingpropertytype);
        }

        //
        // GET: /ListingPropertyType/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ListingPropertyType/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ListingPropertyType listingpropertytype)
        {
            if (ModelState.IsValid)
            {
                db.ListingPropertyTypes.Add(listingpropertytype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(listingpropertytype);
        }

        //
        // GET: /ListingPropertyType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ListingPropertyType listingpropertytype = db.ListingPropertyTypes.Find(id);
            if (listingpropertytype == null)
            {
                return HttpNotFound();
            }
            return View(listingpropertytype);
        }

        //
        // POST: /ListingPropertyType/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ListingPropertyType listingpropertytype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(listingpropertytype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(listingpropertytype);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}