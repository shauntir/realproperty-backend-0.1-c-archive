﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman, Admin")]
    public class FileContentController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /FileContent/

        public ActionResult Index()
        {
            var filecontents = db.FileContents.Include(f => f.UploadUser).Include(f => f.Gallery).Include(f => f.FileContentCategory);
            return View(filecontents.ToList());
        }

        //
        // GET: /FileContent/Details/5

        public ActionResult Details(int id = 0)
        {
            FileContent filecontent = db.FileContents.Find(id);
            if (filecontent == null)
            {
                return HttpNotFound();
            }
            return View(filecontent);
        }

        //
        // GET: /FileContent/Create

        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName");
            ViewBag.GalleryId = new SelectList(db.Galleries, "GalleryId", "Description");
            ViewBag.FileContentCategoryId = new SelectList(db.FileContentCategories, "FileContentCategoryId", "Description");
            return View();
        }

        //
        // POST: /FileContent/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FileContent filecontent)
        {
            if (ModelState.IsValid)
            {
                db.FileContents.Add(filecontent);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", filecontent.UserId);
            ViewBag.GalleryId = new SelectList(db.Galleries, "GalleryId", "Description", filecontent.GalleryId);
            ViewBag.FileContentCategoryId = new SelectList(db.FileContentCategories, "FileContentCategoryId", "Description", filecontent.FileContentCategoryId);
            return View(filecontent);
        }

        //
        // GET: /FileContent/Edit/5

        public ActionResult Edit(int id = 0)
        {
            FileContent filecontent = db.FileContents.Find(id);
            if (filecontent == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", filecontent.UserId);
            ViewBag.GalleryId = new SelectList(db.Galleries, "GalleryId", "Description", filecontent.GalleryId);
            ViewBag.FileContentCategoryId = new SelectList(db.FileContentCategories, "FileContentCategoryId", "Description", filecontent.FileContentCategoryId);
            return View(filecontent);
        }

        //
        // POST: /FileContent/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FileContent filecontent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(filecontent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", filecontent.UserId);
            ViewBag.GalleryId = new SelectList(db.Galleries, "GalleryId", "Description", filecontent.GalleryId);
            ViewBag.FileContentCategoryId = new SelectList(db.FileContentCategories, "FileContentCategoryId", "Description", filecontent.FileContentCategoryId);
            return View(filecontent);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}