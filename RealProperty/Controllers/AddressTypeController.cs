﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman")]
    public class AddressTypeController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /AddressType/

        public ActionResult Index()
        {
            return View(db.AddressTypes.ToList());
        }

        //
        // GET: /AddressType/Details/5

        public ActionResult Details(int id = 0)
        {
            AddressType addresstype = db.AddressTypes.Find(id);
            if (addresstype == null)
            {
                return HttpNotFound();
            }
            return View(addresstype);
        }

        //
        // GET: /AddressType/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /AddressType/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AddressType addresstype)
        {
            if (ModelState.IsValid)
            {
                db.AddressTypes.Add(addresstype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(addresstype);
        }

        //
        // GET: /AddressType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            AddressType addresstype = db.AddressTypes.Find(id);
            if (addresstype == null)
            {
                return HttpNotFound();
            }
            return View(addresstype);
        }

        //
        // POST: /AddressType/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AddressType addresstype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(addresstype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(addresstype);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}