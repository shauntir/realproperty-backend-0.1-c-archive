﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman")]
    public class CustomerTypeController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /CustomerType/

        public ActionResult Index()
        {
            return View(db.CustomerTypes.ToList());
        }

        //
        // GET: /CustomerType/Details/5

        public ActionResult Details(int id = 0)
        {
            CustomerType customertype = db.CustomerTypes.Find(id);
            if (customertype == null)
            {
                return HttpNotFound();
            }
            return View(customertype);
        }

        //
        // GET: /CustomerType/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CustomerType/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerType customertype)
        {
            if (ModelState.IsValid)
            {
                db.CustomerTypes.Add(customertype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(customertype);
        }

        //
        // GET: /CustomerType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CustomerType customertype = db.CustomerTypes.Find(id);
            if (customertype == null)
            {
                return HttpNotFound();
            }
            return View(customertype);
        }

        //
        // POST: /CustomerType/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustomerType customertype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customertype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customertype);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}