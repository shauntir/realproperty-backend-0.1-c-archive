﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman")]
    public class ListingPropertyFeatureController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /ListingPropertyFeature/

        public ActionResult Index()
        {
            var listingpropertyfeatures = db.ListingPropertyFeatures.Include(l => l.ListingPropertyFeatureType).Include(l => l.ListingProperty);
            return View(listingpropertyfeatures.ToList());
        }

        //
        // GET: /ListingPropertyFeature/Details/5

        public ActionResult Details(int id = 0)
        {
            ListingPropertyFeature listingpropertyfeature = db.ListingPropertyFeatures.Find(id);
            if (listingpropertyfeature == null)
            {
                return HttpNotFound();
            }
            return View(listingpropertyfeature);
        }

        //
        // GET: /ListingPropertyFeature/Create

        public ActionResult Create()
        {
            ViewBag.ListingPropertyFeatureTypeId = new SelectList(db.ListingPropertyFeatureTypes, "ListingPropertyFeatureTypeId", "Description");
            ViewBag.ListingPropertyId = new SelectList(db.ListingProperties, "ListingPropertyId", "Title");
            return View();
        }

        //
        // POST: /ListingPropertyFeature/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ListingPropertyFeature listingpropertyfeature)
        {
            if (ModelState.IsValid)
            {
                db.ListingPropertyFeatures.Add(listingpropertyfeature);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ListingPropertyFeatureTypeId = new SelectList(db.ListingPropertyFeatureTypes, "ListingPropertyFeatureTypeId", "Description", listingpropertyfeature.ListingPropertyFeatureTypeId);
            ViewBag.ListingPropertyId = new SelectList(db.ListingProperties, "ListingPropertyId", "Title", listingpropertyfeature.ListingPropertyId);
            return View(listingpropertyfeature);
        }

        //
        // GET: /ListingPropertyFeature/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ListingPropertyFeature listingpropertyfeature = db.ListingPropertyFeatures.Find(id);
            if (listingpropertyfeature == null)
            {
                return HttpNotFound();
            }
            ViewBag.ListingPropertyFeatureTypeId = new SelectList(db.ListingPropertyFeatureTypes, "ListingPropertyFeatureTypeId", "Description", listingpropertyfeature.ListingPropertyFeatureTypeId);
            ViewBag.ListingPropertyId = new SelectList(db.ListingProperties, "ListingPropertyId", "Title", listingpropertyfeature.ListingPropertyId);
            return View(listingpropertyfeature);
        }

        //
        // POST: /ListingPropertyFeature/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ListingPropertyFeature listingpropertyfeature)
        {
            if (ModelState.IsValid)
            {
                db.Entry(listingpropertyfeature).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ListingPropertyFeatureTypeId = new SelectList(db.ListingPropertyFeatureTypes, "ListingPropertyFeatureTypeId", "Description", listingpropertyfeature.ListingPropertyFeatureTypeId);
            ViewBag.ListingPropertyId = new SelectList(db.ListingProperties, "ListingPropertyId", "Title", listingpropertyfeature.ListingPropertyId);
            return View(listingpropertyfeature);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}