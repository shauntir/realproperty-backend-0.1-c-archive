﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;

namespace RealProperty.Controllers
{
    public class ListingTypeController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /ListinyType/

        public ActionResult Index()
        {
            var listingtypes = db.ListingTypes.Include(l => l.LastModifiedUser);
            return View(listingtypes.ToList());
        }

        //
        // GET: /ListinyType/Details/5

        public ActionResult Details(int id = 0)
        {
            ListingType listingtype = db.ListingTypes.Find(id);
            if (listingtype == null)
            {
                return HttpNotFound();
            }
            return View(listingtype);
        }

        //
        // GET: /ListinyType/Create

        public ActionResult Create()
        {
            ViewBag.LastModifiedUserId = new SelectList(db.Users, "UserId", "UserName");
            return View();
        }

        //
        // POST: /ListinyType/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ListingType listingtype)
        {
            if (ModelState.IsValid)
            {
                db.ListingTypes.Add(listingtype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LastModifiedUserId = new SelectList(db.Users, "UserId", "UserName", listingtype.LastModifiedUserId);
            return View(listingtype);
        }

        //
        // GET: /ListinyType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ListingType listingtype = db.ListingTypes.Find(id);
            if (listingtype == null)
            {
                return HttpNotFound();
            }
            ViewBag.LastModifiedUserId = new SelectList(db.Users, "UserId", "UserName", listingtype.LastModifiedUserId);
            return View(listingtype);
        }

        //
        // POST: /ListinyType/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ListingType listingtype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(listingtype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LastModifiedUserId = new SelectList(db.Users, "UserId", "UserName", listingtype.LastModifiedUserId);
            return View(listingtype);
        }

        //
        // GET: /ListinyType/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ListingType listingtype = db.ListingTypes.Find(id);
            if (listingtype == null)
            {
                return HttpNotFound();
            }
            return View(listingtype);
        }

        //
        // POST: /ListinyType/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ListingType listingtype = db.ListingTypes.Find(id);
            db.ListingTypes.Remove(listingtype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}