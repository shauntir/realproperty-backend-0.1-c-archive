﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.ViewModels;
using RealProperty.Domain.Models;
using AutoMapper;
using System.IO;
using System.Web.Security;
using RealProperty.Domain.Helpers;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman, Admin")]
    public class ListingPropertyController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        private IQueryable GetAgents()
        {
            var agents = from u in db.Users
                         join a in db.Agents
                         on u.UserId equals a.UserId
                         select new { a.AgentId, u.UserName };
            return agents;
        }
        private void PopulateCreateFields(ListingPropertyViewModel listingpropertyviewmodel)
        {
            var files = new List<ListingPropertyImagesViewModel> { };
            for (int i = 0; i <= 5; i++)
            {
                files.Add(new ListingPropertyImagesViewModel { });
            }
            listingpropertyviewmodel.ListingPropertyImagesViewModels = files;

            var features = (from l in db.ListingPropertyFeatureTypes
                            select new ListingPropertyFeatureViewModel
                            {
                                ListingPropertyFeatureTypeId = l.ListingPropertyFeatureTypeId,
                                Description = l.Description,
                                AmountOfFeatures = Decimal.Zero
                            }).ToList();
            listingpropertyviewmodel.ListingPropertyFeatureViewModels = features;

            ViewBag.ListingPropertyTypeId = new SelectList(db.ListingPropertyTypes, "ListingPropertyTypeId", "Description");
            ViewBag.ListingTypeId = new SelectList(db.ListingTypes, "ListingTypeId", "Description");
            ViewBag.AddressTypeId = new SelectList(db.AddressTypes, "AddressTypeId", "Description");
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "Description");

            var agents = GetAgents();
            ViewBag.AgentId = new SelectList(agents, "AgentId", "UserName");
        }

        private void PopulateEditFields(ListingPropertyEditViewModel listingpropertyviewmodel, ListingProperty listing)
        {
            List<ListingPropertyImagesViewModel> images =
                         (from f in db.FileContents
                          join l in db.ListingProperties on
                             f.GalleryId equals l.GalleryId
                          where f.FileContentCategory.Description == "Listing Images" &&
                          l.ListingPropertyId == listing.ListingPropertyId
                          select new ListingPropertyImagesViewModel
                          {
                              UploadName = f.UploadName,
                              MaskedName = f.MaskedName,
                              Url = f.Url,
                              FileExtension = f.FileExtension,
                              UserId = f.UserId,
                              GalleryId = f.GalleryId,
                              FileContentCategoryId = f.FileContentCategoryId,
                              FileContentId = f.FileContentId,
                              RemoveFile = false
                          }).ToList();


            int currentNoImages = images.Count();
            for (int i = 0; i <= 5 - currentNoImages; i++)
            {
                images.Add(new ListingPropertyImagesViewModel { });
            }
            listingpropertyviewmodel.ListingPropertyImagesViewModels = images;

            //listingpropertyviewmodel.ListingPropertyTypes = db.ListingPropertyTypes.ToList();

            var features = (from l in db.ListingPropertyFeatureTypes
                            join lpf in db.ListingPropertyFeatures on
                                new { l.ListingPropertyFeatureTypeId, listing.ListingPropertyId } equals
                                new { lpf.ListingPropertyFeatureTypeId, lpf.ListingPropertyId } into ListingFeatures
                            from feat in ListingFeatures.DefaultIfEmpty()
                            select new ListingPropertyFeatureViewModel
                            {
                                ListingPropertyFeatureId = feat.ListingPropertyFeatureId,
                                ListingPropertyFeatureTypeId = l.ListingPropertyFeatureTypeId,
                                Description = l.Description,
                                AmountOfFeatures = (feat.AmountOfFeatures == null ? Decimal.Zero : feat.AmountOfFeatures)
                            }).ToList();
            listingpropertyviewmodel.ListingPropertyFeatureViewModels = features;

            ViewBag.ListingPropertyTypeId = new SelectList(db.ListingPropertyTypes, "ListingPropertyTypeId", "Description", listing.ListingPropertyType.ListingPropertyTypeId);
            ViewBag.ListingTypeId = new SelectList(db.ListingTypes, "ListingTypeId", "Description", listing.ListingType.ListingTypeId);
            ViewBag.AddressTypeId = new SelectList(db.AddressTypes, "AddressTypeId", "Description", listing.Address.AddressTypeId);
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "Description", listing.Address.ProvinceId);

            var agents = GetAgents();
            ViewBag.AgentId = new SelectList(agents, "AgentId", "UserName", listing.Agent.AgentId);
        }

        //
        // GET: /ListingProperty/

        public ActionResult Index()
        {
            var listings = db.ListingProperties.Where(l => l.InActive == false).ToList();
            return View(listings);
        }

        //
        // GET: /ListingProperty/Create

        public ActionResult Create()
        {
            ListingPropertyViewModel listingpropertyviewmodel = new ListingPropertyViewModel();
            PopulateCreateFields(listingpropertyviewmodel);

            return View(listingpropertyviewmodel);
        }

        private FileContent UploadImage(ListingPropertyImagesViewModel i, ListingProperty listingProperty, FileContentCategory fileCategory, Gallery gallery)
        {
            i.UploadName = i.ListingImage.FileName;
            string fileExt = i.ListingImage.FileName.Split('.').LastOrDefault();
            i.FileExtension = fileExt;
            i.MaskedName = Guid.NewGuid().ToString() + "." + fileExt;
            i.Url = Server.MapPath("~/Content/property/images/");
            i.FileContentCategory = fileCategory;

            var fileName = Path.GetFileName(i.ListingImage.FileName);
            var path = Path.Combine(Server.MapPath("~/Content/property/images"), i.MaskedName);
            i.ListingImage.SaveAs(path);
            i.GalleryId = gallery.GalleryId;
            //listingProperty.GalleryId = i.GalleryId;

            FileContent image;
            AutoMapper.Mapper.CreateMap<ListingPropertyImagesViewModel, FileContent>();
            image = AutoMapper.Mapper.Map<ListingPropertyImagesViewModel, FileContent>(i);

            return image;
        }

        // POST: /ListingProperty/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ListingPropertyViewModel listingpropertyviewmodel)
        {

            if (ModelState.IsValid)
            {
                AutoMapper.Mapper.CreateMap<ListingPropertyViewModel, ListingProperty>();
                var listingProperty = AutoMapper.Mapper.Map<ListingPropertyViewModel, ListingProperty>(listingpropertyviewmodel);

                db.Addresses.Add(listingpropertyviewmodel.Address);

                var fileCategory = db.FileContentCategories.Where(fc => fc.Description == "Listing Images").SingleOrDefault();
                var gallery = new Gallery { Description = fileCategory.Description + "_" + listingProperty.Title + "_" + DateTime.Today.ToShortDateString() };
                db.Galleries.Add(gallery);

                listingProperty.GalleryId = gallery.GalleryId;
                db.ListingProperties.Add(listingProperty);
                
                foreach (var feature in listingpropertyviewmodel.ListingPropertyFeatureViewModels)
                {
                    var f = new ListingPropertyFeature 
                    {
                        AmountOfFeatures = feature.AmountOfFeatures,
                        ListingPropertyFeatureTypeId = feature.ListingPropertyFeatureTypeId,
                        ListingPropertyId  = listingProperty.ListingPropertyId
                    };
                    db.ListingPropertyFeatures.Add(f);
                }
                
                foreach (var i in listingpropertyviewmodel.ListingPropertyImagesViewModels)
                {
                    if (i.ListingImage != null)
                    {
                        i.UserId = DataAccess.GetCurrentUserId() == null ? 0 : DataAccess.GetCurrentUserId().Value;;
                        var image = UploadImage(i, listingProperty, fileCategory, gallery);
                        db.FileContents.Add(image);
                    }
                    
                }
                
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            PopulateCreateFields(listingpropertyviewmodel);
            return View(listingpropertyviewmodel);
        }

        //
        // GET: /ListingProperty/Edit

        public ActionResult Edit(int id=0)
        {
            var listing = db.ListingProperties.Find(id);
            if (listing == null)
            {
                return HttpNotFound();
            }
            ListingPropertyEditViewModel listingpropertyviewmodel = new ListingPropertyEditViewModel();

            AutoMapper.Mapper.CreateMap<ListingProperty, ListingPropertyEditViewModel>();
            listingpropertyviewmodel = AutoMapper.Mapper.Map<ListingProperty, ListingPropertyEditViewModel>(listing);

            PopulateEditFields(listingpropertyviewmodel, listing);
            return View(listingpropertyviewmodel);
        }

        //
        // POST: /ListingProperty/Edit

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ListingPropertyEditViewModel listingpropertyviewmodel)
        {
            if (ModelState.IsValid)
            {
                //ListingProperty listingProperty;// = db.ListingProperties.Find(listingpropertyviewmodel.ListingPropertyId);
                AutoMapper.Mapper.CreateMap<ListingPropertyEditViewModel, ListingProperty>();
                var listingProperty = AutoMapper.Mapper.Map<ListingPropertyEditViewModel, ListingProperty>(listingpropertyviewmodel);

                //Address address;// = db.Addresses.Find(listingpropertyviewmodel.AddressId);
                AutoMapper.Mapper.CreateMap<Address, Address>();
                var address = AutoMapper.Mapper.Map<Address, Address>(listingpropertyviewmodel.Address);
                address.AddressId = listingpropertyviewmodel.AddressId;

                listingProperty.Address = address;
                listingProperty.AddressId = address.AddressId;

                db.Entry(address).State = EntityState.Modified;
                db.SaveChanges();

                db.Entry(listingProperty).State = EntityState.Modified;
                db.SaveChanges();

                var gallery = db.Galleries.Find(listingpropertyviewmodel.GalleryId);
                foreach (var feature in listingpropertyviewmodel.ListingPropertyFeatureViewModels)
                {
                    var f = db.ListingPropertyFeatures.Find(feature.ListingPropertyFeatureId);
                    f.AmountOfFeatures = feature.AmountOfFeatures;
                    f.ListingPropertyFeatureTypeId = feature.ListingPropertyFeatureTypeId;
                    
                    db.Entry(f).State = EntityState.Modified;
                    
                    db.SaveChanges();
                }

                var fileCategory = db.FileContentCategories.Where(fc => fc.Description == "Listing Images").SingleOrDefault();
                foreach (var i in listingpropertyviewmodel.ListingPropertyImagesViewModels)
                {
                    if (i.ListingImage != null && i.FileContentId > 0) //update
                    {
                        var existingFile = db.FileContents.Find(i.FileContentId);

                        System.IO.File.Delete(Path.Combine(Server.MapPath("~/Content/property/images"), existingFile.MaskedName)); //Delete existing image
                        db.Entry(existingFile).State = EntityState.Detached;

                        existingFile = UploadImage(i, listingProperty, fileCategory, gallery);
                        existingFile.Gallery = gallery;
                        existingFile.GalleryId = gallery.GalleryId;
                        existingFile.FileContentCategoryId = fileCategory.FileContentCategoryId;

                        db.Entry(existingFile).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else if (i.ListingImage != null && i.FileContentId == 0) //insert
                    {
                        var image = UploadImage(i, listingProperty, fileCategory, gallery);

                        db.FileContents.Add(image);
                        db.SaveChanges();
                    }

                    else if (i.RemoveFile && i.FileContentId > 0) //delete
                    {
                        var existingFile = db.FileContents.Find(i.FileContentId);
                        System.IO.File.Delete(Path.Combine(Server.MapPath("~/Content/property/images"), existingFile.MaskedName)); //Delete existing image
                        db.FileContents.Remove(existingFile);
                        db.SaveChanges();
                    }
                }


                return RedirectToAction("Index");
            }

            var listing = db.ListingProperties.Find(listingpropertyviewmodel.ListingPropertyId);

            AutoMapper.Mapper.CreateMap<ListingProperty, ListingPropertyEditViewModel>();
            listingpropertyviewmodel = AutoMapper.Mapper.Map<ListingProperty, ListingPropertyEditViewModel>(listing);

            PopulateEditFields(listingpropertyviewmodel, listing);
            return View(listingpropertyviewmodel);
        }

        // GET: /ListingProperty/Deactivate/id

        public ActionResult Deactivate(int id = 0)
        {
            var listing = db.ListingProperties.Find(id);
            if (listing == null)
            {
                return HttpNotFound();
            }
            else 
            {
                listing.InActive = true;
                db.Entry(listing).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /ListingProperty/Details

        public ActionResult Details(int id = 0)
        {
            var listing = db.ListingProperties.Find(id);
            if (listing == null)
            {
                return HttpNotFound();
            }
            ListingPropertyEditViewModel listingpropertyviewmodel = new ListingPropertyEditViewModel();

            AutoMapper.Mapper.CreateMap<ListingProperty, ListingPropertyEditViewModel>();
            listingpropertyviewmodel = AutoMapper.Mapper.Map<ListingProperty, ListingPropertyEditViewModel>(listing);

            PopulateEditFields(listingpropertyviewmodel, listing);
            return View(listingpropertyviewmodel);
        }

        public ActionResult ListingEnquiries(int listingPropertyId)
        {
            var enquiries = db.ListingPropertyEnquiry.Where(e => e.ListingPropertyId == listingPropertyId);

            return View(enquiries);
        }

        public ActionResult ListingEnquiryDetails(int id = 0)
        {
            var listingEnquiry = db.ListingPropertyEnquiry.Find(id);
            if (listingEnquiry == null)
            {
                return HttpNotFound();
            }

            return View(listingEnquiry);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}