﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RealProperty.Domain.Models;

namespace RealProperty.Controllers
{
    [Authorize(Roles = "Superman, Admin")]
    public class AddressController : Controller
    {
        private RealPropertyContext db = new RealPropertyContext();

        //
        // GET: /Address/

        public ActionResult Index()
        {
            var addresses = db.Addresses.Include(a => a.AddressType).Include(a => a.Province);
            return View(addresses.ToList());
        }

        //
        // GET: /Address/Details/5

        public ActionResult Details(int id = 0)
        {
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        //
        // GET: /Address/Create

        public ActionResult Create()
        {
            ViewBag.AddressTypeId = new SelectList(db.AddressTypes, "AddressTypeId", "Description");
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "Description");
            return View();
        }

        //
        // POST: /Address/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Address address)
        {
            if (ModelState.IsValid)
            {
                db.Addresses.Add(address);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AddressTypeId = new SelectList(db.AddressTypes, "AddressTypeId", "Description", address.AddressTypeId);
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "Description", address.ProvinceId);
            return View(address);
        }

        //
        // GET: /Address/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            ViewBag.AddressTypeId = new SelectList(db.AddressTypes, "AddressTypeId", "Description", address.AddressTypeId);
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "Description", address.ProvinceId);
            return View(address);
        }

        //
        // POST: /Address/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Address address)
        {
            if (ModelState.IsValid)
            {
                db.Entry(address).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AddressTypeId = new SelectList(db.AddressTypes, "AddressTypeId", "Description", address.AddressTypeId);
            ViewBag.ProvinceId = new SelectList(db.Provinces, "ProvinceId", "Description", address.ProvinceId);
            return View(address);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}