﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class ListingPropertyFeature
    {
        public int ListingPropertyFeatureId { get; set; }

        public decimal AmountOfFeatures { get; set; }

        public int ListingPropertyFeatureTypeId { get; set; }
        public virtual ListingPropertyFeatureType ListingPropertyFeatureType { get; set; }

        public int ListingPropertyId { get; set; }
        public virtual ListingProperty ListingProperty { get; set; }
    }
}