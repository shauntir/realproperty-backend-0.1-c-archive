﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class OfficeContactNumber : BaseContactNumber
    {
        [Column(Order = 1), Key, ForeignKey("Office")]
        public int OfficeId { get; set; }
        public virtual Office Office { get; set; }
    }
}