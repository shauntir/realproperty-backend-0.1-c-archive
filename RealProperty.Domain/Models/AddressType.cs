﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class AddressType
    {
        public int AddressTypeId { get; set; }

        [Display(Name = "Address Type")]
        [Required(ErrorMessage = "Address type is required.")]
        [StringLength(50, ErrorMessage = "Please enter a value no more than 50 characters.")]
        public String Description { get; set; }

        public virtual ICollection<Address> Address { get; set; }
    }
}