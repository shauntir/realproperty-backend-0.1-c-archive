﻿using RealProperty.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Spatial;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class Office
    {
        public int OfficeId { get; set; }

        [Display(Name = "Office Name")]
        [Required(ErrorMessage = "Office name is required.")]
        [StringLength(50, ErrorMessage = "Please enter a value no more than 50 characters.")]
        public String Name { get; set; }

        [Display(Name = "GPS Location")]
        public DbGeography Location { get; set; }

        public int? MainOfficeId { get; set; }
        [ForeignKey("MainOfficeId")]
        public virtual Office MainOffice { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool HideAddress { get; set; }

        private DateTime _lastModified = DateTime.Now;
        [DataType(DataType.DateTime)]
        public DateTime DateTimeModified
        {
            get { return this._lastModified; }
            set { this._lastModified = value; }
        }

        [ForeignKey("LastModifiedUserId")]
        public virtual User LastModifiedUser { get; set; }
        public int? LastModifiedUserId { get { return DataAccess.GetCurrentUserId(); } set { value = DataAccess.GetCurrentUserId(); } }

        public virtual Address Address { get; set; }
        public int AddressId { get; set; }

        public virtual ICollection<Agent> Agent { get; set; }
    }
}