﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class CustomerType
    {
        public int CustomerTypeId { get; set; }

        [Display(Name = "Customer Type")]
        [Required(ErrorMessage = "Customer type is required.")]
        [StringLength(50, ErrorMessage = "Please enter a value no more than 50 characters.")]
        public String Description { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}