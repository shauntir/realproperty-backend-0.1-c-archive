﻿using RealProperty.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class FileContent
    {
        public int FileContentId { get; set; }

        [Display(Name = "Upload Name")]
        [Required(ErrorMessage = "Upload name is required.")]
        [StringLength(200, ErrorMessage = "Please enter a value no more than 200 characters.")]
        public String UploadName { get; set; }

        [Display(Name = "Masked Name")]
        [Required(ErrorMessage = "Masked name is required.")]
        [StringLength(41, ErrorMessage = "Please enter a value no more than 41 characters.")]
        public String MaskedName { get; set; }

        [Display(Name = "Url")]
        [Required(ErrorMessage = "Url is required.")]
        [DataType(DataType.Url)]
        public String Url { get; set; }

        [Display(Name = "File Extension")]
        [Required(ErrorMessage = "File extension is required.")]
        [StringLength(4, ErrorMessage = "Please enter a value no more than 4 characters.")]
        public String FileExtension { get; set; }

        [ForeignKey("UserId")]
        public virtual User UploadUser { get; set; }
        public int UserId { 
            get { return DataAccess.GetCurrentUserId() == null ? 0 : DataAccess.GetCurrentUserId().Value; }
            set { value = DataAccess.GetCurrentUserId() == null ? 0 : DataAccess.GetCurrentUserId().Value; } 
        }

        public virtual Gallery Gallery { get; set; }
        public int GalleryId { get; set; }

        public virtual FileContentCategory FileContentCategory { get; set; }
        public int FileContentCategoryId { get; set; }

        private DateTime _lastModified = DateTime.Now;
        [DataType(DataType.DateTime)]
        public DateTime DateTimeModified
        {
            get { return this._lastModified; }
            set { this._lastModified = value; }
        }

        [ForeignKey("LastModifiedUserId")]
        public virtual User LastModifiedUser { get; set; }
        public int? LastModifiedUserId { get { return DataAccess.GetCurrentUserId(); } set { value = DataAccess.GetCurrentUserId(); } }
    }
}