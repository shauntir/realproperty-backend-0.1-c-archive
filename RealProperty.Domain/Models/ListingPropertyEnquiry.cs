﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class ListingPropertyEnquiry
    {
        public int ListingPropertyEnquiryId { get; set; }

        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        public int ListingPropertyId { get; set; }
        public virtual ListingProperty ListingProperty { get; set; }

        [Display(Name = "Comment")]
        [Required(ErrorMessage = "Comment in required.")]
        [StringLength(500, ErrorMessage = "Please enter a value no more than 500 characters.")]
        public String Comment { get; set; }
    }
}