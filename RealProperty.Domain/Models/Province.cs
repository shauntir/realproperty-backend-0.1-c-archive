﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class Province
    {
        public int ProvinceId { get; set; }

        [Display(Name = "Provice")]
        [Required(ErrorMessage = "Province is required.")]
        [StringLength(50, ErrorMessage = "Please enter a value no more than 50 characters.")]
        public String Description { get; set; }

        public int CountryId { get; set; }
        public virtual Country Country { get; set; }

        public virtual ICollection<Address> Address { get; set; }
    }
}