﻿using RealProperty.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class ListingProperty
    {
        public int ListingPropertyId { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is required.")]
        [StringLength(50, ErrorMessage = "Please enter a value no more than 50 characters.")]
        public String Title { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description is required.")]
        [StringLength(200, ErrorMessage = "Please enter a value no more than 200 characters.")]
        public String Description { get; set; }

        [Display(Name = "Extras")]
        [StringLength(200, ErrorMessage = "Please enter a value no more than 200 characters.")]
        public String Extras { get; set; }

        [Display(Name = "Reference Number")]
        [Required(ErrorMessage = "Reference is required.")]
        [StringLength(12, ErrorMessage = "Please enter a value no more than 12 characters.")]
        public String ReferenceNumber { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool Featured { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool DisplayAddress { get; set; }

        [Required]
        [DefaultValue(0)]
        public int NumberOfClicks  { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool InActive { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool Sold { get; set; }

        [Display(Name = "Selling Price")]
        [Required(ErrorMessage = "Selling price is required.")]
        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal SellingPrice { get; set; }

        public int AddressId { get; set; }
        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; }
        
        public virtual Agent Agent { get; set; }
        public int AgentId { get; set; }

        public virtual Gallery Gallery { get; set; }
        public int? GalleryId { get; set; }

        public virtual ListingPropertyType ListingPropertyType { get; set; }
        public int ListingPropertyTypeId { get; set; }

        public virtual ListingType ListingType { get; set; }
        public int ListingTypeId { get; set; }

        public virtual ICollection<ListingPropertyFeature> ListingPropertyFeatures { get; set; }
        public virtual ICollection<ListingPropertyEnquiry> ListingPropertyEnquies { get; set; }

        private DateTime _lastModified = DateTime.Now;
        [DataType(DataType.DateTime)]
        public DateTime DateTimeModified
        {
            get { return this._lastModified; }
            set { this._lastModified = value; }
        }

        [ForeignKey("LastModifiedUserId")]
        public virtual User LastModifiedUser { get; set; }
        public int? LastModifiedUserId { get { return DataAccess.GetCurrentUserId(); } set { value = DataAccess.GetCurrentUserId(); } }

        //[Timestamp]
        //public virtual byte[] Timestamp { get; set; }
    }
}