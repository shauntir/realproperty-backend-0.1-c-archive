﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class FileContentCategory
    {
        public int FileContentCategoryId { get; set; }

        [Display(Name = "File Content Category")]
        [Required(ErrorMessage = "File content category is required.")]
        [StringLength(20, ErrorMessage = "Please enter a value no more than 20 characters.")]
        public String Description { get; set; }

        public virtual ICollection<FileContent> FileContent { get; set; }
    }
}