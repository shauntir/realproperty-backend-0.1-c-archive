﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Spatial;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class Company
    {
        public int CompanyId { get; set; }

        [Display(Name = "Company Name")]
        [Required(ErrorMessage = "Company name is required.")]
        [StringLength(50, ErrorMessage = "Please enter a value no more than 50 characters.")]
        public String Name { get; set; }

        private DateTime _lastModified = DateTime.Now;
        [DataType(DataType.DateTime)]
        public DateTime DateTimeModified
        {
            get { return this._lastModified; }
            set { this._lastModified = value; }
        }

        public virtual ICollection<User> User { get; set; }
    }
}