﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public abstract class BaseContactNumber
    {
        [Column(Order = 0), Key, ForeignKey("ContactNumberType")]
        public int ContactNumberTypeId { get; set; }
        public virtual ContactNumberType ContactNumberType { get; set; }

        [Display(Name = "Contact Number")]
        [Required(ErrorMessage = "Contact number is required.")]
        [StringLength(11, ErrorMessage = "Please enter a value no more than 11 characters.")]
        public String ContactNumber { get; set; }
    }
}