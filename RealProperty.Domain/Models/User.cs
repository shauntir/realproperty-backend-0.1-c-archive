﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web.Security;

namespace RealProperty.Domain.Models
{

    [Table("User")]
    public class User
    {
        public int UserId { get; set; }

        [Display(Name = "User name")]
        [Required(ErrorMessage = "Usernname is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string UserName { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First name is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last name is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [StringLength(254, ErrorMessage = "Please enter a value no more than 254 characters.")]
        public string Email { get; set; }

        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public int CompanyId { get; set; }

        private DateTime _lastModified = DateTime.Now;
        [DataType(DataType.DateTime)]
        public DateTime DateTimeModified
        {
            get { return this._lastModified; }
            set { this._lastModified = value; }
        }

        public virtual ICollection<FileContent> FileContent { get; set; }
    }

    public class EditUserViewModel
    {
        public int UserId { get; set; }

        [Display(Name = "User name")]
        [Required(ErrorMessage = "Usernname is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string UserName { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First name is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last name is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [StringLength(254, ErrorMessage = "Please enter a value no more than 254 characters.")]
        public string Email { get; set; }

        public int CompanyId { get; set; }
    }

    public class UserCreate
    {
        [Display(Name = "User name")]
        [Required(ErrorMessage = "Usernname is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string UserName { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First name is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last name is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [StringLength(254, ErrorMessage = "Please enter a value no more than 254 characters.")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public virtual Company Company { get; set; }
        public int CompanyId { get; set; }

        private DateTime _lastModified = DateTime.Now;
        [DataType(DataType.DateTime)]
        public DateTime DateTimeModified
        {
            get { return this._lastModified; }
            set { this._lastModified = value; }
        }
    }
    public class UserLogin
    {
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }


    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}