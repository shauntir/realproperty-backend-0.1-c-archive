﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class ContactNumberType
    {
        public int ContactNumberTypeId { get; set; }

        [Display(Name = "Contact number type")]
        [Required(ErrorMessage = "Contact number type is required.")]
        [StringLength(20, ErrorMessage = "Please enter a value no more than 20 characters.")]
        public String Description { get; set; }
    }
}