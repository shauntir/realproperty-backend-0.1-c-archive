﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealProperty.Domain.Models
{
    public class Country
    {
        public int CountryId { get; set; }

        [Display(Name = "Country Name")]
        [Required(ErrorMessage = "Country name is required.")]
        [StringLength(50, ErrorMessage = "Please enter a value no more than 50 characters.")]
        public String Name { get; set; }

        public virtual ICollection<Province> Provinces { get; set; }
    }
}
