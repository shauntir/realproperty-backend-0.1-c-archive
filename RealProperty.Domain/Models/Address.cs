﻿using RealProperty.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Spatial;
using System.Drawing;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class Address
    {
        public int AddressId { get; set; }
        
        [Display(Name = "Street Number")]
        [Required(ErrorMessage = "Street number is required.")]
        public Int16 StreetNumber { get; set; }

        [Display(Name = "Street Name")]
        [Required(ErrorMessage = "Street name is required.")]
        [StringLength(180, ErrorMessage = "Please enter a value no more than 180 characters.")]
        public String StreetName { get; set; }

        [Display(Name = "Area")]
        [Required(ErrorMessage = "Area is required.")]
        [StringLength(150, ErrorMessage = "Please enter a value no more than 150 characters.")]
        public String Area { get; set; }

        [Display(Name = "Suburb")]
        [StringLength(150, ErrorMessage = "Please enter a value no more than 150 characters.")]
        public String Suburb { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "City is required.")]
        [StringLength(100, ErrorMessage = "Please enter a value no more than 100 characters.")]
        public String City { get; set; }

        [Display(Name = "Code")]
        [DataType(DataType.PostalCode)]
        [StringLength(4, ErrorMessage = "Please enter a value no more than 4 characters.")]
        public String Code { get; set; }

        [Display(Name = "GPS Location")]
        public DbGeography Location { get; set; }
        
        public virtual AddressType AddressType { get; set; }
        public int AddressTypeId { get; set; }

        public virtual Province Province { get; set; }
        public int ProvinceId { get; set; }

        private DateTime _lastModified = DateTime.Now;
        [DataType(DataType.DateTime)]
        public DateTime DateTimeModified
        {
            get { return this._lastModified; }
            private set { this._lastModified = value; }
        }

        [ForeignKey("LastModifiedUserId")]
        public virtual User LastModifiedUser { get; set; }
        public int? LastModifiedUserId { get { return DataAccess.GetCurrentUserId(); } set { value = DataAccess.GetCurrentUserId(); } }

        public virtual ICollection<Office> Offices { get; set; }
    }
}