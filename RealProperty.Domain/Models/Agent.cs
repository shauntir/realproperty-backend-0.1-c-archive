﻿using RealProperty.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class Agent
    {
        public int AgentId { get; set; }

        public virtual Office Office { get; set; }
        public int OfficeId { get; set; }

        public virtual User User { get; set; }
        public int UserId { get; set; }

        private DateTime _lastModified = DateTime.Now;
        [DataType(DataType.DateTime)]
        public DateTime DateTimeModified
        {
            get { return this._lastModified; }
            set { this._lastModified = value; }
        }

        public virtual ICollection<ListingProperty> ListingProperties { get; set; }
        public virtual ICollection<AgentContactNumber> AgentContactNumbers { get; set; }
    }

    public class CreateAgentViewModel
    {

        [Display(Name = "User name")]
        [Required(ErrorMessage = "Usernname is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string UserName { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First name is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last name is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [StringLength(254, ErrorMessage = "Please enter a value no more than 254 characters.")]
        public string Email { get; set; }

        public int CompanyId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public int OfficeId { get; set; }

        public virtual List<ContactNumberViewModel> ContactNumberViewModels { get; set; }
    }

    public class EditAgentViewModel : EditUserViewModel
    {
        public int AgentId { get; set; }
        public int OfficeId { get; set; }
        
        [Display(Name = "User name")]
        [Required(ErrorMessage = "Usernname is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string UserName { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First name is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last name is required.")]
        [StringLength(80, ErrorMessage = "Please enter a value no more than 80 characters.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [StringLength(254, ErrorMessage = "Please enter a value no more than 254 characters.")]
        public string Email { get; set; }

        public int CompanyId { get; set; }

        public virtual List<ContactNumberViewModel> ContactNumberViewModels { get; set; }
    }
}