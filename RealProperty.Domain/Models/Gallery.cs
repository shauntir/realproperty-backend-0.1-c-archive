﻿using RealProperty.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealProperty.Domain.Models
{
    public class Gallery
    {
        public int GalleryId { get; set; }

        [Display(Name = "Gallery")]
        [Required(ErrorMessage = "Gallery description is required.")]
        [StringLength(50, ErrorMessage = "Please enter a value no more than 50 characters.")]
        public String Description { get; set; }

        private DateTime _lastModified = DateTime.Now;
        [DataType(DataType.DateTime)]
        public DateTime DateTimeModified
        {
            get { return this._lastModified; }
            set { this._lastModified = value; }
        }

        [ForeignKey("LastModifiedUserId")]
        public virtual User LastModifiedUser { get; set; }
        public int? LastModifiedUserId { get { return DataAccess.GetCurrentUserId(); } set { value = DataAccess.GetCurrentUserId(); } }

        public virtual ICollection<FileContent> FileContent { get; set; }
        public virtual ICollection<ListingProperty> ListingProperties { get; set; }
    }
}