﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using RealProperty.Domain.Helpers;

namespace RealProperty.Domain.Models
{
    public class ListingPropertyType
    {
        public int ListingPropertyTypeId { get; set; }

        [Display(Name="Property Type")]
        [Required(ErrorMessage="Property type is required.")]
        [StringLength(50, ErrorMessage="Please enter a value no more than 50 characters.")]
        public String Description { get; set; }

        public virtual ICollection<ListingProperty> ListingProperties { get; set; }

        private DateTime _lastModified = DateTime.Now;
        [DataType(DataType.DateTime)]
        public DateTime DateTimeModified
        {
            get { return this._lastModified; }
            set { this._lastModified = value; }
        }

        [ForeignKey("LastModifiedUserId")]
        public virtual User LastModifiedUser { get; set; }
        public int? LastModifiedUserId { get { return DataAccess.GetCurrentUserId(); } set { value = DataAccess.GetCurrentUserId(); } }
    }
}