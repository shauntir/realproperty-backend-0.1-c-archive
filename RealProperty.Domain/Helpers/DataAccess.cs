﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace RealProperty.Domain.Helpers
{
    public static class DataAccess
    {
        public static int? GetCurrentUserId()
        {
            try
            {
                MembershipUser user = Membership.GetUser();
                string id = user.ProviderUserKey.ToString();
                return int.Parse(id);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
