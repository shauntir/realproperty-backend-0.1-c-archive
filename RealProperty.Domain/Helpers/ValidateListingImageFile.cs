﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using RealProperty.Domain.ViewModels;

namespace RealProperty.Domain.Helpers
{
    /// <summary>
    /// Adapted from: http://stackoverflow.com/questions/6388812/how-to-validate-uploaded-file-in-asp-net-mvc/6388927#6388927
    /// </summary>
    public class ValidateListingImageFile : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var files = (value as List<ListingPropertyImagesViewModel>);
            
            foreach (var file in files.Where(f => f.ListingImage != null))
            {
                var f = file.ListingImage;
                if (f.ContentLength > 3 * 1024 * 1024)
                {
                    return false;
                }

                try
                {
                    using (var img = Image.FromStream(f.InputStream))
                    {
                        var imgType = img.RawFormat.GetType().Equals(typeof(ImageFormat));
                        if (img.Width < 400 && img.Height < 300) 
                        {
                            return false;
                        }
                    }

                }
                catch 
                {
                    return false; 
                }
            }
            return true;
        }
    }
}
