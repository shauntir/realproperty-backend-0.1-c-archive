﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using RealProperty.Domain.ViewModels;
using AutoMapper;

namespace RealProperty.Domain.Models
{
    public class RealPropertyContext : DbContext
    {
        public RealPropertyContext()
            : base("RealProp")
        {
        }

        public DbSet<AddressType> AddressTypes { get; set; }
        public DbSet<ListingPropertyFeature> ListingPropertyFeatures { get; set; }
        public DbSet<ListingPropertyFeatureType> ListingPropertyFeatureTypes { get; set; }
        public DbSet<ListingPropertyType> ListingPropertyTypes { get; set; }
        public DbSet<ListingType> ListingTypes { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<CustomerType> CustomerTypes { get; set; }
        public DbSet<ContactNumberType> ContactNumberTypes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<FileContentCategory> FileContentCategories { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<FileContent> FileContents { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<ListingProperty> ListingProperties { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<ListingPropertyEnquiry> ListingPropertyEnquiry { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<AgentContactNumber> AgentContactNumbers { get; set; }
        public DbSet<CustomerContactNumber> CustomerContactNumbers { get; set; }
        public DbSet<OfficeContactNumber> OfficeContactNumbers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //Remove cascade delete on address from property
            /*
            modelBuilder.Entity<ListingProperty>()
                .HasRequired(lp => lp.Address)
                .WithMany(a => a.ListingProperties)
                .HasForeignKey(lp => lp.AddressId)
                .WillCascadeOnDelete(false);
            */
            modelBuilder.Entity<ListingProperty>()
                        .HasRequired(lp => lp.Address)
                        .WithMany()
                        .HasForeignKey(lp => lp.AddressId).WillCascadeOnDelete(false);

            modelBuilder.Entity<ListingPropertyEnquiry>()
                .HasRequired(lpe => lpe.ListingProperty)
                .WithMany(lp => lp.ListingPropertyEnquies)
                .HasForeignKey(lpe => lpe.ListingPropertyId)
                .WillCascadeOnDelete(false);

                
            //Remove cascade delete on agent from property
            modelBuilder.Entity<ListingProperty>()
                .HasRequired(lp => lp.Agent)
                .WithMany(a => a.ListingProperties)
                .HasForeignKey(lp => lp.AgentId)
                .WillCascadeOnDelete(false);

            //Remove cascade delete on user from property
            modelBuilder.Entity<ListingProperty>()
                .HasOptional(lp => lp.LastModifiedUser)
                .WithMany()
                .HasForeignKey(lp => lp.LastModifiedUserId)
                .WillCascadeOnDelete(false);

            //Remove cascade delete on user from gallery
            modelBuilder.Entity<Gallery>()
                .HasOptional(g => g.LastModifiedUser)
                .WithMany()
                .HasForeignKey(g => g.LastModifiedUserId)
                .WillCascadeOnDelete(false);

            //Remove cascade delete on user from office
            modelBuilder.Entity<Office>()
                .HasOptional(o => o.LastModifiedUser)
                .WithMany()
                .HasForeignKey(o => o.LastModifiedUserId)
                .WillCascadeOnDelete(false);

            //Remove cascade delete on address from office
            modelBuilder.Entity<Office>()
                .HasRequired(o => o.Address)
                .WithMany(a => a.Offices)
                .HasForeignKey(o => o.AddressId)
                .WillCascadeOnDelete(false);

            //Remove cascade delete on user from feature type
            modelBuilder.Entity<ListingPropertyFeatureType>()
                .HasOptional(lp => lp.LastModifiedUser)
                .WithMany()
                .HasForeignKey(lp => lp.LastModifiedUserId)
                .WillCascadeOnDelete(false);

            //Remove cascade delete on user from filecontent
            modelBuilder.Entity<FileContent>()
                .HasOptional(f => f.LastModifiedUser)
                .WithMany()
                .HasForeignKey(f => f.LastModifiedUserId)
                .WillCascadeOnDelete(false);

            

            base.OnModelCreating(modelBuilder);
        }
    }
}