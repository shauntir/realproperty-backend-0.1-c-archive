﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Spatial;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;

namespace RealProperty.Domain.Models
{
    public class RealPropertyInitializer : DropCreateDatabaseAlways<RealPropertyContext>
    {
        protected override void Seed(RealPropertyContext context)
        {
            if (!WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("RealProp", "User", "UserId", "UserName", autoCreateTables: true);
            }
            
            context.Database.ExecuteSqlCommand("ALTER TABLE ListingProperty ADD CONSTRAINT UC_ListingProperty_AddressId UNIQUE(AddressId)");
            context.Database.ExecuteSqlCommand("ALTER TABLE Agent ADD CONSTRAINT UC_ListingProperty_UserId UNIQUE(UserId)");

            var company = new Company
            {
                Name = "NHF"
            };
            context.Companies.Add(company);
            context.SaveChanges();

            var user = new UserCreate
            {
                UserName = "Shauntir",
                FirstName = "Shaun",
                LastName = "Tirubeni",
                Email = "shauntir@gmail.com",
                Password = "1987SHN1209",
                CompanyId = company.CompanyId
            };
            WebSecurity.CreateUserAndAccount(user.UserName, user.Password, new { user.Email, user.FirstName, user.LastName, user.CompanyId, user.DateTimeModified });

            Roles.CreateRole("Admin");
            Roles.CreateRole("Superman");
            Roles.AddUsersToRole(new[] { user.UserName }, "Superman");
            Roles.AddUsersToRole(new[] { user.UserName }, "Admin");

            var standardUser = new UserCreate
            {
                UserName = "standardUser",
                FirstName = "standardUser",
                LastName = "standardUser",
                Email = "standardUser@gmail.com",
                Password = "standardUser",
                CompanyId = company.CompanyId
            };
            WebSecurity.CreateUserAndAccount(standardUser.UserName, standardUser.Password, new { standardUser.Email, standardUser.FirstName, standardUser.LastName, standardUser.CompanyId, standardUser.DateTimeModified });
            Roles.AddUsersToRole(new[] { standardUser.UserName }, "Admin");

            var country = new Country { Name = "South Africa" };
            context.Countries.Add(country);

            var provinces = new List<Province> 
            { 
                new Province { Description = "Eastern Cape", Country = country },
                new Province { Description = "Free State", Country = country },
                new Province { Description = "Gauteng", Country = country },
                new Province { Description = "KwaZulu-Natal", Country = country },
                new Province { Description = "Limpopo", Country = country },
                new Province { Description = "Mpumalanga", Country = country },
                new Province { Description = "North West", Country = country },
                new Province { Description = "Northern Cape", Country = country },
                new Province { Description = "Western Cape", Country = country },
            };
            provinces.ForEach(p => context.Provinces.Add(p));

            var addressTypes = new List<AddressType> 
            { 
                new AddressType { Description = "Residential" },
                new AddressType { Description = "Work" },
                new AddressType { Description = "Postal" },
                new AddressType { Description = "Other" },
            };
            addressTypes.ForEach(at => context.AddressTypes.Add(at));

            var fileContentCategories = new List<FileContentCategory> 
            { 
                new FileContentCategory { Description = "Listing Images" },
                new FileContentCategory { Description = "Listing Documents" },
                new FileContentCategory { Description = "Branch Images" },
                new FileContentCategory { Description = "Branch Documents" },
            };
            fileContentCategories.ForEach(fc => context.FileContentCategories.Add(fc));

            var customerTypes = new List<CustomerType> 
            { 
                new CustomerType { Description = "Website Lead" },
                new CustomerType { Description = "Phone Call Lead" },
                new CustomerType { Description = "Email Lead" },
                new CustomerType { Description = "Existing" },
            };
            customerTypes.ForEach(ct => context.CustomerTypes.Add(ct));

            var customerContactNumberTypes = new List<ContactNumberType> 
            { 
                new ContactNumberType { Description = "Cell" },
                new ContactNumberType { Description = "Home" },
                new ContactNumberType { Description = "Work" },
                new ContactNumberType { Description = "Fax" },
                new ContactNumberType { Description = "Other" },
            };
            customerContactNumberTypes.ForEach(ccnt => context.ContactNumberTypes.Add(ccnt));

            var listingPropertyType = new List<ListingPropertyType> 
            { 
                new ListingPropertyType { Description = "House", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingPropertyType { Description = "Apartment", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingPropertyType { Description = "Townhouse", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingPropertyType { Description = "Vacant Land / Plot", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingPropertyType { Description = "Farm", LastModifiedUser = context.Users.FirstOrDefault() },
            };
            listingPropertyType.ForEach(lpt => context.ListingPropertyTypes.Add(lpt));

            var listingType = new List<ListingType> 
            { 
                new ListingType { Description = "Residential For Sale", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingType { Description = "Residential To Let", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingType { Description = "New Development", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingType { Description = "Commercial For Sale", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingType { Description = "Commercial To Let", LastModifiedUser = context.Users.FirstOrDefault() },
            };
            listingType.ForEach(lt => context.ListingTypes.Add(lt));

            var listingPropertyFeatureType = new List<ListingPropertyFeatureType> 
            { 
                new ListingPropertyFeatureType { Description = "Beds", LastModifiedUser = context.Users.FirstOrDefault()},
                new ListingPropertyFeatureType { Description = "Bathrooms", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingPropertyFeatureType { Description = "Garages", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingPropertyFeatureType { Description = "Pools", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingPropertyFeatureType { Description = "Living Areas", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingPropertyFeatureType { Description = "Kitchen", LastModifiedUser = context.Users.FirstOrDefault() },
                new ListingPropertyFeatureType { Description = "Car Ports", LastModifiedUser = context.Users.FirstOrDefault() },
            };
            listingPropertyFeatureType.ForEach(lpft => context.ListingPropertyFeatureTypes.Add(lpft));

            var address = new Address
            {
                StreetNumber = 69,
                StreetName = "Woodview Drive",
                Area = "Woodview",
                Suburb = "Phoenix",
                City = "Durban",
                Code = "4068",
                Location = DbGeography.PointFromText("POINT(-122.31249 47.632342)", 4326),
                AddressType = new AddressType { Description = "Other" },
                Province = new Province { Description = "Gauteng" },
                LastModifiedUser = context.Users.FirstOrDefault()
            };
            context.Addresses.Add(address);


            var office = new Office 
            {
                Name = "Estate Office",
                Address = address,
                LastModifiedUser = context.Users.FirstOrDefault()
            };
            context.Offices.Add(office);

            var agent = new Agent 
            { 
                User = context.Users.Where(x => x.UserName == "Shauntir").FirstOrDefault(),
                Office = office,
            };
            context.Agents.Add(agent);

            context.SaveChanges();
            base.Seed(context);
        }
    }
}