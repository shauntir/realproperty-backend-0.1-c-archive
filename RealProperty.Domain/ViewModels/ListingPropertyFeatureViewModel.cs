﻿using DataAnnotationsExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealProperty.Domain.ViewModels
{
    public class ListingPropertyFeatureViewModel
    {
        public int ListingPropertyFeatureId { get; set; }

        [Required]
        public int ListingPropertyFeatureTypeId { get; set; }
        
        public String Description { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        public decimal AmountOfFeatures { get; set; }
    }
}
