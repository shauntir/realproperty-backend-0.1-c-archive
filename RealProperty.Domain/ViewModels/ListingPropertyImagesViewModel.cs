﻿using RealProperty.Domain.Helpers;
using RealProperty.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebMatrix.WebData;

namespace RealProperty.Domain.ViewModels
{
    public class ListingPropertyImagesViewModel
    {
        public int FileContentId { get; set; }

        [Display(Name = "Upload Name")]
        [StringLength(200, ErrorMessage = "Please enter a value no more than 200 characters.")]
        public String UploadName { get; set; }

        [Display(Name = "Masked Name")]
        [StringLength(41, ErrorMessage = "Please enter a value no more than 41 characters.")]
        public String MaskedName { get; set; }

        [Display(Name = "Url")]
        [DataType(DataType.Url)]
        public String Url { get; set; }

        [Display(Name = "File Extension")]
        [StringLength(4, ErrorMessage = "Please enter a value no more than 4 characters.")]
        public String FileExtension { get; set; }

        public virtual User UploadUser { get; set; }
        public int UserId { get; set; }

        //public virtual Gallery Gallery { get; set; }
        public int GalleryId { get; set; }

        public virtual FileContentCategory FileContentCategory { get; set; }
        public int FileContentCategoryId { get; set; }

        public bool RemoveFile { get; set; }

        public HttpPostedFileBase ListingImage { get; set; }
    }
}
