﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RealProperty.Domain.ViewModels
{
    public class ContactNumberViewModel
    {
        [Required]
        public int ContactNumberTypeId { get; set; }
        
        public String Description { get; set; }

        [Display(Name = "Contact Number")]
        [StringLength(11, ErrorMessage = "Please enter a value no more than 11 characters.")]
        public String ContactNumber { get; set; }
    }
}
