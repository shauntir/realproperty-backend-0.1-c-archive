﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealProperty.Domain.Models;
using System.Web;
using RealProperty.Domain.Helpers;
using System.ComponentModel.DataAnnotations.Schema;

namespace RealProperty.Domain.ViewModels
{
    public class ListingPropertyViewModel
    {
        public int ListingPropertyId { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is required.")]
        [StringLength(50, ErrorMessage = "Please enter a value no more than 50 characters.")]
        public String Title { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description is required.")]
        [StringLength(200, ErrorMessage = "Please enter a value no more than 200 characters.")]
        public String Description { get; set; }

        [Display(Name = "Extras")]
        [StringLength(200, ErrorMessage = "Please enter a value no more than 200 characters.")]
        public String Extras { get; set; }

        [Display(Name = "Reference Number")]
        [Required(ErrorMessage = "Reference is required.")]
        [StringLength(12, ErrorMessage = "Please enter a value no more than 12 characters.")]
        public String ReferenceNumber { get; set; }

        [Display(Name = "Selling Price")]
        [Required(ErrorMessage = "Selling price is required.")]
        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal SellingPrice { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool Featured { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool DisplayAddress { get; set; }


        [Required]
        public virtual Address Address { get; set; }

        [Required]
        public int AgentId { get; set; }

        public int ListingPropertyTypeId { get; set; }

        public int ListingTypeId { get; set; }

        [Required]
        public virtual List<ListingPropertyFeatureViewModel> ListingPropertyFeatureViewModels { get; set; }

        [ValidateListingImageFile(ErrorMessage = "Please select IMAGES only that are at least 300x400 and smaller than 3MB.")]
        public List<ListingPropertyImagesViewModel> ListingPropertyImagesViewModels { get; set; }
        //public virtual ListingPropertyType ListingPropertyType { get; set; }
        //[UIHint("ListingPropertyType")]
        //public virtual ICollection<ListingPropertyType> ListingPropertyTypes { get; set; }
    }

    public class ListingPropertyEditViewModel : ListingPropertyViewModel
    {
        public int AddressId { get; set; }
        public int GalleryId { get; set; }
        [Required]
        [DefaultValue(false)]
        public bool Sold { get; set; }
    }
}
